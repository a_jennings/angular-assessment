import { Component, OnInit } from '@angular/core';
import { ServiceService } from './service.service';
import { getLocaleDateTimeFormat } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  // Declare models
  data:Object[] = []
  frmId:number = 1
  choices:Object[] = [
    {cat:'people'},
    {cat:'planets'},
    {cat:'vehicles'},
    {cat:'species'},
    {cat:'starships'}
  ]
  whichCategory:string = 'people'
  model
  searches:string[] = []
  showFlag = false;

  // Declare functions
  constructor(private service:ServiceService){

  }

  // Show 'Search history' button when Submit is clicked
  showSearch(){
    this.showFlag = true;
  }

  // When 'submit' is clicked:
  // 1. Add search to search array
  // 2. Send and retrieve API all from service
  handleClick(){
    this.searches.push(`id = ${this.frmId}; category = ${this.whichCategory}; Search time = ${new Date()}`)
    this.service.getData(this.frmId, this.whichCategory)
      .subscribe( (result) => {
        this.model = result
      })
  }


}
