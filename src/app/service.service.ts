import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  // Declare models
  apiUrl:string = 'https://swapi.co/api/';
  data:any

  // Declare methods for this service

  // Make and return an API call with the users search entries
  getData(id, category){
    return this.http.get(`${this.apiUrl}${category}/${id}`)
    .pipe(
      catchError(this.handleError<Object[]>('getData', []))
    )
  }

  // Handle errors, mainly if the API call doesn't work
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // Log error to console
      console.error(error); 

    // Let the app keep running by returning an empty result.
    return of(result as T);
    };
  }

  constructor(private http:HttpClient) { }
}
